const state = {
  spuId: '',
  spuName: ''
}
const actions = {}
const mutations = {
  SET_SPU_INFO(state, spuInfo) {
    state.spuId = spuInfo.id
    state.spuName = spuInfo.spuName
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}

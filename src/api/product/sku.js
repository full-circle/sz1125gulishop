import request from '@/utils/request'
// 1. 添加SKU
export function reqSaveSkuInfo(data) {
  return request({
    url: `/admin/product/saveSkuInfo`,
    method: 'post',
    data
  })
}
// 2. 查找所有的sku数据列表
export function reqGetSkuList(spuId) {
  return request({
    url: `/admin/product/findBySpuId/${spuId}`, // 自己线上真正的接口地址
    method: 'get'
  })
}
// 3. 查找所有的SKU列表
export function reqGetAllSkuList({ page, limit }) {
  return request({
    url: `/admin/product/list/${page}/${limit}`, // 自己线上真正的接口地址
    method: 'get'
  })
}

// 4. SKU上架
export function reqSkuOnSale(skuId) {
  return request({
    url: `/admin/product/onSale/${skuId}`, // 自己线上真正的接口地址
    method: 'get'
  })
}

// 5. SKU下架
export function reqSkuCancelSale(skuId) {
  return request({
    url: `/admin/product/cancelSale/${skuId}`, // 自己线上真正的接口地址
    method: 'get'
  })
}
// 6. 删除
export function reqDeleteSku(skuId) {
  return request({
    url: `/admin/product/deleteSku/${skuId}`, // 自己线上真正的接口地址
    method: 'delete'
  })
}
// 7. 获取当前sku信息
export function reqGetOneSkuInfo(skuId) {
  return request({
    url: `/admin/product/getSkuById/${skuId}`, // 自己线上真正的接口地址
    method: 'get'
  })
}

// 8. 更新sKu
export function reqUpdateSkuInfo(data) {
  return request({
    url: `/admin/product/updateSkuInfo`,
    method: 'post',
    data
  })
}

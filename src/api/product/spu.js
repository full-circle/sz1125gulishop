
import request from '@/utils/request'

// 1. 获取一级分类列表
export function reqGetSpuList({ page, limit, category3Id }) {
  return request({
    url: `/admin/product/${page}/${limit}`,
    method: 'get',
    params: { // parameter的缩写 是参数的意思
      category3Id
    }
  })
}
// 2. 获取trademark数据
export function reqGetAllTrademarkList() {
  return request({
    url: `/admin/product/baseTrademark/getTrademarkList`,
    method: 'get'
  })
}

// 3. 获取属性数据
export function reqGetBaseSaleAttrList() {
  return request({
    url: `/admin/product/baseSaleAttrList`,
    method: 'get'
  })
}

// 4. 添加SPU
export function reqSaveSpuInfo(data) {
  return request({
    url: `/admin/product/saveSpuInfo`,
    method: 'post',
    data
  })
}

// 5. 根据spuId获取照片墙列表 其实就是那些图片
export function reqGetSpuImageList(spuId) {
  return request({
    url: `/admin/product/spuImageList/${spuId}`,
    method: 'get'
  })
}

// 6. 根据SPUId获取销售属性及属性值
export function reqGetSpuSaleAttrList(spuId) {
  return request({
    url: `/admin/product/spuSaleAttrList/${spuId}`,
    method: 'get'
  })
}

// 4. 更新SPU
export function reqUpdateSpuInfo(data) {
  return request({
    url: `/admin/product/updateSpuInfo`,
    method: 'post',
    data
  })
}
// 5. 删除一条spu数据

export function reqDeleteSpuInfo(spuId) {
  return request({
    url: `/admin/product/deleteSpu/${spuId}`,
    method: 'delete'
  })
}
